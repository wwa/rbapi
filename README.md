# rbapi

## What is it?
rbapi is a minimal viable trading engine/API for video games.
It supports full accounting, limit orders, partial fills, taxes and more.
It's also blazing fast!

## Getting started
Build:

```cmake rbapi && make```

Run unit tests:

```./test```

## Requirements
Linux (for now), cmake 3.10+, c++17, boost

## Contact
Any suggestions? Contact us at:
dev@realmsbridge.com
https://realmsbridge.com
