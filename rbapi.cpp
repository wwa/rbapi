#include <rbapi.h>
#include <map>
#include <unordered_map>
#include <fstream>
#include <iostream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/composite_key.hpp>
using boost::multi_index_container;
using namespace boost::multi_index;
namespace RealmsBridge {
struct ctx_t;
extern ctx_t ctx;
struct tag_oid{};
struct tag_aid{};
struct tag_prc{};
typedef uint64_t pid_t;
static inline std::tuple<sid_t,sid_t> pid2sid(pid_t p)
{
  sid_t s1 = (p>>0 )&0xFFFFFFFFull;
  sid_t s2 = (p>>32)&0xFFFFFFFFull;
  return std::make_tuple(s1,s2);
}
static inline pid_t sid2pid(sid_t s1, sid_t s2)
{
  return (((pid_t)s2)<<32)|((pid_t)s1);
}
struct Order_t
{
  friend class boost::serialization::access;
  pid_t pid;
  oid_t oid;
  aid_t aid;
  prc_t prc;
  amt_t amt;
  Order_t(){}
  Order_t(pid_t pid_ ,oid_t oid_, aid_t aid_, prc_t prc_, amt_t amt_):
          pid  (pid_),oid  (oid_),aid  (aid_),prc  (prc_),amt  (amt_){}
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version)
  {
    ar & pid;
    ar & oid;
    ar & aid;
    ar & prc;
    ar & amt;
  }
};
struct Account_t
{
  friend class boost::serialization::access;
  aid_t aid;
  sid_t sid;
  mutable amt_t amt;
  Account_t(){}
  Account_t(aid_t u,  sid_t p,  amt_t a):
            aid  (u), sid  (p), amt  (a){}
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version)
  {
    ar & aid;
    ar & sid;
    ar & amt;
  }
};
template <typename C>
struct ModAmt {
  ModAmt(const amt_t& _a):a(_a){
  }
  void operator()(C& o){
    o.amt = a;
  }
private:
  amt_t a;
};
typedef composite_key<
  Order_t,
  BOOST_MULTI_INDEX_MEMBER(Order_t, aid_t, aid),
  BOOST_MULTI_INDEX_MEMBER(Order_t, pid_t, pid),
  BOOST_MULTI_INDEX_MEMBER(Order_t, oid_t, oid)
> aid_key;
typedef composite_key_compare<
  std::less<aid_t>,
  std::less<pid_t>,
  std::less<oid_t>
> aid_cmp;
typedef composite_key<
  Order_t,
  BOOST_MULTI_INDEX_MEMBER(Order_t,pid_t,pid),
  BOOST_MULTI_INDEX_MEMBER(Order_t,prc_t,prc),
  BOOST_MULTI_INDEX_MEMBER(Order_t,oid_t,oid)
> prc_key;
struct prc_cmp {
  static bool compare(pid_t a_x, prc_t b_x, oid_t c_x,
                      pid_t a_y, prc_t b_y, oid_t c_y) {
    //TODO: amt_t <> 0
    if(a_x==a_y) {
      if(b_x==b_y) {
        return c_x < c_y;
      }
      return b_x < b_y;
    }
    return a_x < a_y;
  }
  static bool compare(pid_t a_x, int b_x,
                      pid_t a_y, int b_y) {
    if(a_x==a_y) {
      return b_x < b_y;
    }
    return a_x < a_y;
  }
  static bool compare(pid_t a_x,
                      pid_t a_y) {
    return a_x < a_y;
  }
  bool operator()( 
    const boost::multi_index::composite_key_result<prc_key>& x,
    const boost::multi_index::composite_key_result<prc_key>& y) const
  {
    return compare(x.value.pid,x.value.prc,x.value.oid,
                   y.value.pid,y.value.prc,y.value.oid);
  }
  bool operator()( 
    const boost::multi_index::composite_key_result<prc_key>& x,
    const boost::tuple<const pid_t,const prc_t>& y) const
  {
    return compare(x.value.pid,  x.value.prc,
                   y.get<0>(),   y.get<1>());
  }
  bool operator()( 
    const boost::tuple<const pid_t,const prc_t>& x,
    const boost::multi_index::composite_key_result<prc_key>& y) const
  {
    return compare(x.get<0>(),   x.get<1>(),
                   y.value.pid,  y.value.prc);
  }
  bool operator()( 
    const boost::multi_index::composite_key_result<prc_key>& x,
    const pid_t& y) const
  {
    return compare(x.value.pid, y);
  }
  bool operator()( 
    const pid_t& x,
    const boost::multi_index::composite_key_result<prc_key>& y) const
  {
    return compare(x, y.value.pid);
  }
};
typedef multi_index_container<
  Order_t,
  indexed_by<
    hashed_unique     <tag<tag_oid>, BOOST_MULTI_INDEX_MEMBER(Order_t,oid_t,oid)>,
    ordered_unique    <tag<tag_prc>, prc_key, prc_cmp>,
    ordered_unique    <tag<tag_aid>, aid_key, aid_cmp>
>> orders_t;
typedef composite_key<
  Account_t,
  BOOST_MULTI_INDEX_MEMBER(Account_t,aid_t,aid),
  BOOST_MULTI_INDEX_MEMBER(Account_t,sid_t,sid)
> act_key;
typedef composite_key_compare<
  std::less<aid_t>,
  std::less<sid_t>
> act_cmp;
typedef multi_index_container<
  Account_t,
  indexed_by<
    ordered_unique    <tag<tag_aid>, act_key, act_cmp>
>> accounts_t;
struct ctx_t {
  friend class boost::serialization::access;
  oid_t      oidseq;
  aid_t      aidseq;
  orders_t   orders;
  accounts_t accounts;
  ctx_t(){}
  template <class Archive>
  void serialize(Archive & ar, const unsigned int version)
  {
    ar & oidseq;
    ar & aidseq;
    ar & orders;
    ar & accounts;
  }
  void clear()
  {
    orders.clear();
    accounts.clear();
  }
  void load(const std::string& fname)
  {
    std::ifstream ifs(fname);
    boost::archive::text_iarchive ia(ifs);
    ia>>*this;
  }
  void save(const std::string& fname)
  {
    std::ofstream ofs(fname);
    boost::archive::text_oarchive oa(ofs);
    oa<<*this;
  }
};
namespace Context {
  // NOTE: testmode only
  void Clear()
  {
    ctx.clear();
  }
  void Load(const std::string& fname)
  {
    ctx.load(fname);
  }
  void Save(const std::string& fname)
  {
    ctx.save(fname);
  }
}
// -------------- API starts here --------------
namespace Account {
  std::tuple<res_t,aid_t> Create()
  {
    aid_t aid = ctx.aidseq++;
    return std::make_tuple(RB_OK,aid);
  }
  std::tuple<res_t,amt_t> Balance(aid_t aid, sid_t sid, amt_t amt)
  {
    res_t res = RB_ERR_UNKNOWN;
    auto  ait = ctx.accounts.get<tag_aid>().find(boost::make_tuple(aid,sid));
    if(ait != ctx.accounts.get<tag_aid>().end())
    {
      amt += ait->amt;
      if(amt == 0) {
        ctx.accounts.get<tag_aid>().erase(ait);
      }
      else {
        ctx.accounts.get<tag_aid>().modify(ait,ModAmt<Account_t>(amt));
      }
      res = RB_OK;
    }
    else if (amt > 0)
    {
      ctx.accounts.get<tag_aid>().insert(Account_t(aid,sid,amt));
      res = RB_OK;
    }
    return std::make_tuple(res,amt);
  }
  std::tuple<res_t,std::string> Link()
  {
    return std::make_tuple(RB_ERR_TODO, std::string());
  }
}  // namespace Order
namespace Order {
  std::tuple<res_t,oid_t> Create(aid_t aid, sid_t sid1, sid_t sid2, amt_t amt, ord_t type, prc_t prc)
  {
    if(sid1 > sid2)
    {
      std::swap(sid1,sid2);
      amt = -amt;
    }
    if (amt < 0)
    {
      amt = -amt;
      prc = -prc;
    }
    pid_t pid = sid2pid(sid1,sid2);
    res_t res = RB_ERR_UNKNOWN;
    oid_t oid = ctx.oidseq++;
    if(MARKET == type)
    {
      res = RB_ERR_TODO;
    }
    else
    {
      auto oit = ctx.orders.get<tag_prc>().lower_bound(boost::make_tuple(pid,-prc));
      while(oit != ctx.orders.get<tag_prc>().end())
      {
        if (prc > 0 && oit->prc >= 0)
        {
          break;
        }
        auto amt1 = std::min(amt, oit->amt);
        auto amt2 = amt1 * oit->prc;
        auto tax1 = amt1 * taxrate;
        auto tax2 = amt2 * taxrate;
        Account::Balance(0,        sid1,  tax1);
        Account::Balance(0,        sid2,  tax2);
        Account::Balance(aid,      sid1, -amt1);
        Account::Balance(aid,      sid2,  amt2-tax2);
        Account::Balance(oit->aid, sid1,  amt1-tax1);
        Account::Balance(oit->aid, sid2, -amt2);
        if(amt1 == oit->amt)
        {
          oit = ctx.orders.get<tag_prc>().erase(oit);
        }
        else
        {
          ctx.orders.get<tag_prc>().modify(oit,ModAmt<Order_t>(oit->amt-amt1));
          oit++;
        }
        amt -= amt1;
        if(amt == 0)
        {
          break;
        }
      }
      if(amt != 0)
      {
        ctx.orders.get<tag_oid>().insert(Order_t(pid, oid, aid, prc, amt));
      }
      res = RB_OK;
    }
    return std::make_tuple(res,oid);
  }
  std::tuple<res_t> Cancel(oid_t oid)
  {
    auto oit = ctx.orders.get<tag_oid>().find(oid);
    if(oit == ctx.orders.get<tag_oid>().end())
    {
      return RB_ERR_NOTFOUND;
    }
    else
    {
      ctx.orders.get<tag_oid>().erase(oit);
    }
    return std::make_tuple(RB_OK);
  }
  std::tuple<res_t,amt_t> Status(oid_t oid)
  {
    amt_t amt = 0;
    res_t res = RB_ERR_NOTFOUND;
    auto  oit = ctx.orders.get<tag_oid>().find(oid);
    if(oit != ctx.orders.get<tag_oid>().end())
    {
      amt = oit->amt;
      res = RB_OK;
    }
    return std::make_tuple(res,amt);
  }
} // namespace Order
ctx_t ctx;
} // namespace RealmsBridge

