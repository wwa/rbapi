#include <rbapi.h>
#include <cassert>
#include <iostream>
using namespace RealmsBridge;
void testBalance()
{
  // Create account and deposit. Make sure balance is updated.
  auto s1=1, s2=2;
  res_t res = RB_ERR_UNKNOWN;
  aid_t aid = 0;
  amt_t amt = 0;
  std::tie(res,aid) = Account::Create();             assert(res == RB_OK);
  std::tie(res,amt) = Account::Balance(aid,s1,12.0); assert(res == RB_OK && amt == 12.0);
  std::tie(res,amt) = Account::Balance(aid,s1,11.0); assert(res == RB_OK && amt == 23.0);
  Context::Clear();
}
void testOrder()
{
  // Create a pair of orders to insert into orderbook
  // Create an order to fill against orderbook
  // Make sure orders are taken off the orderbook
  // Make sure balances are updated
  res_t res  = RB_ERR_UNKNOWN;
  sid_t sid1 = 1,sid2 = 2;
  aid_t aid1 = 1,aid2 = 2;
  amt_t amt  = 0;
  oid_t oid1 = 0, oid2 = 0, oid3 = 0;
  std::tie(res,aid1) = Account::Create();                                    assert(res == RB_OK);
  std::tie(res,aid2) = Account::Create();                                    assert(res == RB_OK);
  std::tie(res,amt)  = Account::Balance(aid1,sid2,12.0);                     assert(res == RB_OK && amt == 12.0);
  std::tie(res,amt)  = Account::Balance(aid2,sid1,23.0);                     assert(res == RB_OK && amt == 23.0);
  std::tie(res,oid1) = Order::Create(aid1,sid1,sid2, 2.0,Order::LIMIT, 1.0); assert(res == RB_OK);
  std::tie(res,oid2) = Order::Create(aid1,sid1,sid2, 1.0,Order::LIMIT, 2.0); assert(res == RB_OK);
  std::tie(res,oid3) = Order::Create(aid2,sid1,sid2,-2.0,Order::LIMIT, 1.0); assert(res == RB_OK);
  std::tie(res)      = Order::Cancel(oid1);                                  assert(res == RB_ERR_NOTFOUND);
  std::tie(res)      = Order::Cancel(oid2);                                  assert(res == RB_OK);
  std::tie(res)      = Order::Cancel(oid3);                                  assert(res == RB_ERR_NOTFOUND);
  std::tie(res,amt)  = Account::Balance(0   ,sid1,0);                        assert(res == RB_OK && amt == 2.0*taxrate);
  std::tie(res,amt)  = Account::Balance(0   ,sid2,0);                        assert(res == RB_OK && amt == 2.0*taxrate);
  std::tie(res,amt)  = Account::Balance(aid1,sid1,0);                        assert(res == RB_OK && amt == 2.0*(1.0-taxrate));
  std::tie(res,amt)  = Account::Balance(aid1,sid2,0);                        assert(res == RB_OK && amt == 10.0);
  std::tie(res,amt)  = Account::Balance(aid2,sid1,0);                        assert(res == RB_OK && amt == 21.0);
  std::tie(res,amt)  = Account::Balance(aid2,sid2,0);                        assert(res == RB_OK && amt == 2.0*(1.0-taxrate));
  Context::Clear();
}
int main(void) {
  testBalance();
  testOrder();
  std::cout << "Done." << std::endl;
  return 0;
}
