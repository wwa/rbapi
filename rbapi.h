#pragma once
#include <stdint.h>
#include <tuple>
namespace RealmsBridge {
  typedef uint64_t aid_t;     // account id
  typedef uint32_t sid_t;     // symbol  id (i.e. ticker)
  typedef uint64_t oid_t;     // order   id
  typedef double   amt_t;     // amount type
  typedef double   prc_t;     // price  type
  typedef enum {              // response/error code type
    RB_OK           =  200,   // success
    RB_ERR_UNKNOWN  = -500,   // unexpected error
    RB_ERR_INVALID  = -400,   // invalid argument
    RB_ERR_NOPERM   = -403,   // no permissions
    RB_ERR_NOTFOUND = -404,   // object not found
    RB_ERR_TODO     = -501,   // not implemented
  } res_t;
  const prc_t taxrate = 0.05; // Tax rate. Taxes are credited to aid = 0
  namespace Account {
    // Creates a new account.
    std::tuple<res_t,aid_t> Create();
    
    // Updates and returns account balance. Negative amount means a withdrawal.
    std::tuple<res_t,amt_t> Balance(aid_t aid, sid_t sid, amt_t amount);
    
    // Returns a URL to link the in-game account with RealmsBridge account
    std::tuple<res_t,std::string> Link();
  };
  namespace Order {
    typedef enum {
      LIMIT,
      MARKET
    } ord_t;
    
    // Creates a buy/sell order. Negative amount means sell.
    // Attempts matching against orderbook. If not instantly filled, inserts the order into orderbook.
    // Orders can be partially filled.
    std::tuple<res_t,oid_t> Create(aid_t aid, sid_t sid1, sid_t sid2, amt_t amt, ord_t type, prc_t price);
    
    // Queries order status and returns its remaining amount.
    std::tuple<res_t,amt_t> Status(oid_t oid);
    
    // Cancels an order.
    std::tuple<res_t>       Cancel(oid_t oid);
  };
  namespace Context {
    // Empties account and order records. Test-mode only.
    void Clear();
    
    // Loads context from file. Test-mode only.
    void Load(const std::string& fname);
    
    // Saves context to file. Test-mode only.
    void Save(const std::string& fname); 
  }
  namespace Billing {} // TODO: Records of all executed orders
};
